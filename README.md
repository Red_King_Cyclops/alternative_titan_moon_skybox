# Alternative textures to the Titan Moon skybox

This texture pack retextures titan_moon's skybox. I don't like this skybox as much as the normal one, but I wanted to test out a different method in making the skybox. The saturn image used in the skybox is a modified version of the saturn image Twoelk made here: https://forum.minetest.net/viewtopic.php?p=150727#p150727 .

Media is licenced as CC-BY-SA 3.0.